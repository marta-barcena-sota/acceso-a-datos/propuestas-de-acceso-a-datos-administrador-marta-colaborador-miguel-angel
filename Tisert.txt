Ejemplo de propuesta:

IMPLEMENTACIÓN DE UN SISTEMA DE INFORMACIÓN PARA LA TIENDA DE CAMISETAS “TISERT”

Se desea desarrollar un sistema de información para gestionar los datos de los empleados, secciones, clientes, proveedores y artículos que se venden en Tisert.
En Tisert, los empleados asesoran a los clientes, siendo estos últimos los que llevan a cabo la compra de su/s camiseta/s. 

En lo que concierne a los proveedores, queremos optimizar nuestros precios lo más posible. Cada camiseta nos es ofrecida por varios proveedores y cada proveedor cuenta con un amplio catálogo de camisetas. Hemos detectado que en el tiempo, la misma camiseta es suministrada por un mismo proveedor siempre a precios distintos, por lo que queremos guardar dichos precios para hacer un seguimiento de las tendencias y establecer cuáles son los proveedores que nos ofrecen mejores condiciones. Necesitaremos además conocer el cif, nombre y teléfono del contacto del proveedor, dirección, teléfono y términos de pago.

Tisert identifica sus camisetas por el código de artículo junto con el código de modelo de camiseta. Necesitamos saber además el color, la talla, si es una edición limitada o no, la sección de la tienda a la que pertenece (hombre/mujer/niños) y su precio de venta.

Cuando un cliente accede a nuestra tienda se le asigna un vendedor y cada vez que ese cliente acuda, le atenderá la misma persona. Sin embargo, cada empleado tendrá varios clientes en su agenda, siendo de interés las fechas en las que un vendedor ha atendido a un determinado cliente. 

Sobre nuestros empleados necesitaremos almacenar sus datos personales, antigüedad en la empresa y la comisión que les corresponde. Dicha comisión se calcula en base a la antigüedad. Si el empleado lleva de 1 a 5 meses en la empresa, tendrá comisión de tipo A. Si llevase de 6 a 12 meses, la misión sería de tipo B. Los empleados que superen los 12 meses en nuestra empresa tendrán una comisión de tipo C. La antigüedad se almacenará siempre en meses por motivos administrativos.

La tienda está dividida en varias secciones, que se distinguen por la temática de las camisetas que se pueden encontrar allí. Los empleados pueden estar asignados a más de una sección, ya que nuestra tienda es de pequeñas dimensiones. Sin embargo, por la habitual carga de trabajo pueden encontrarse varios empleados por sección para atender a todos los clientes que acuden.

Una vez un empleado asesora a un cliente, este entra en nuestra base de datos con un código de cliente, datos personales y teléfono, que serán archivados con toda regularidad siguiendo las directrices que establece la agencia española de protección de datos. 

Esperamos que este sistema ayude a centralizar, normalizar y regular la información que manejamos, así como a dar un mejor servicio a nuestros clientes.

Quedo a vuestra disposición para cualquier duda que tengáis.

Buen fin de semana y un saludo.