Buenos días:

No se había especificado dónde incluir la redacción de las 3 propuestas de ampliación para vuestro proyecto de acceso a datos.

Por favor incluidlo en el documento que está listado como número 1 en la lista de documentos a entregar (Documentación del proceso de desarrollo (Redacción del proceso que se ha llevado a cabo para completar el proyecto que podrá contener como figuras los diagramas y deberá contar con una tabla de contenidos))

Aprovecho para comentaros que la entrega del proyecto se abrirá a las 14:05 de mañana viernes 27/11. En dicha entrega deberá haber 8 archivos subidos individualmente (SIN COMPRIMIR).

Por si hay cualquier duda os vuelvo a indicar qué es lo que tenéis que entregar:

1.- Documentación del proceso de desarrollo (Redacción del proceso que se ha llevado a cabo para completar el proyecto que podrá contener como figuras los diagramas y deberá contar con una tabla de contenidos). Además del discurrir del proceso de desarrollo aquí es donde deberéis añadir vuestras propuestas candidatas y elegida, que habrán debido ser validadas previamente y también las 3 propuestas de posibles ampliaciones del sistema de información.

2.- Archivo .dia con el diagrama ER

3.- Archivo que contenga el diagrama relacional (podéis elegir el formato que más os convenga como os he indicado en clase)

4.- Archivo .sql con la creación de tablas y restricciones (según el modelo aportado)

5.- Archivo con la configuración de la generación de los datos (.dgen)

6.- Copia de seguridad de la base de datos con los datos insertados en las tablas (.sql)

7.- Archivo con el diagrama de relaciones generado automáticamente (.dbd)

8.- Archivo .pdf de la documentación generada automáticamente

Sobra decir que si alguien no tiene la propuesta validada tendrá su proyecto automáticamente suspenso como ya dije en clase. Además si no se cumplen las condiciones de la entrega, no sé evaluará el proyecto (énfasis en que los ficheros han de entregarse sin ser un archivo comprimido y sin comprimir individualmente).

Por último, cuidad el estilo de vuestra documentación, no os olvidéis de repasar la ortografía, la gramática y  la puntuación de lo que redactéis. Como os he hecho saber en clase repetidamente lo considero relevante aunque trabajéis en un entorno técnico.